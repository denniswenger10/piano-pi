"use strict";
var _fastify = _interopRequireDefault(require("fastify"));
var _fastifyStatic = _interopRequireDefault(require("fastify-static"));
var _fastifyCors = _interopRequireDefault(require("fastify-cors"));
var _path = _interopRequireDefault(require("path"));
var _v1 = require("./api/v1");
var _piano = require("./piano");
var _environment = require("./environment");
function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
const main = async ()=>{
    const pianoPort = _environment.environment().pianoPort;
    const soundfonts = _environment.environment().soundfontPaths;
    await _piano.startPiano(soundfonts, pianoPort);
    const app = _fastify.default();
    app.register(_fastifyCors.default);
    app.register(_fastifyStatic.default, {
        root: _path.default.join(__dirname, '../../client', 'build'),
        prefix: '/'
    });
    app.register(_v1.instrumentRoutes, {
        prefix: '/api/v1/instruments'
    });
    const port = 3000;
    const start = async ()=>{
        try {
            await app.listen(port);
            console.log(`Server started at ${port}`);
        } catch (err) {
            app.log.error(err);
        }
    };
    start();
};
main();
