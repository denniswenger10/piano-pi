"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.createInstrument = void 0;
var _common = require("../common");
const createInstrument = (instrument = {
})=>{
    const { name ='' , bank =0 , tags =[] , number =0 , id =_common.uniqueId() ,  } = instrument;
    return {
        name,
        bank,
        tags,
        number,
        id
    };
};
exports.createInstrument = createInstrument;
