"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.selectInstrument = void 0;
var _instrumentState = require("./instrument-state");
var _common = require("../common");
var _option = require("../option");
var _piano = require("../piano");
const selectInstrument = async (id)=>{
    const instrumentOption = _common.findWhere(({ id: _id  })=>_id === id
    , _instrumentState.instrumentState.instrumentList.read());
    if (instrumentOption.isNone()) {
        return _option.none();
    }
    const instrument = instrumentOption.unwrap();
    console.time('Fluidsynth');
    const connection = await _piano.fluidsynth();
    console.timeEnd('Fluidsynth');
    console.time('Select instrument');
    await connection.selectInstrument(instrument);
    console.timeEnd('Select instrument');
    _instrumentState.instrumentState.selectedInstrument.update(_option.some(instrument));
    return _option.some(instrument);
};
exports.selectInstrument = selectInstrument;
