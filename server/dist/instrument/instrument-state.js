"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.instrumentState = void 0;
var _option = require("../option");
const state = {
    instrumentList: [],
    selectedInstrument: _option.none()
};
const instrumentState = {
    instrumentList: {
        read: ()=>state.instrumentList
        ,
        update: (list)=>state.instrumentList = list
    },
    selectedInstrument: {
        read: ()=>state.selectedInstrument
        ,
        update: (instrument)=>state.selectedInstrument = instrument
    }
};
exports.instrumentState = instrumentState;
