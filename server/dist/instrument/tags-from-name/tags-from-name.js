"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.tagsFromName = void 0;
var _option = require("../../option");
var _tagFunctions = require("./tag-functions");
const tagsFromName = (name = '')=>{
    const functions = [
        _tagFunctions.fretsTag,
        _tagFunctions.keyboardTag,
        _tagFunctions.noiseTag,
        _tagFunctions.orchestraTag, 
    ];
    const lowerCaseName = name.toLowerCase();
    return _option.filterMap((fn)=>fn(lowerCaseName)
    , functions);
};
exports.tagsFromName = tagsFromName;
