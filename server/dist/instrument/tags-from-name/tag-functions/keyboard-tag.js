"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.keyboardTag = void 0;
var _option = require("../../../option");
var _common = require("../../../common");
const keyboardTag = (name)=>{
    return _common.textIncludesAny([
        'piano',
        'grand',
        'organ',
        ' ep',
        'clavinet',
        'celesta',
        'harpsichord',
        'honky tonk',
        'honky-tonk', 
    ], name) ? _option.some('keyboard') : _option.none();
};
exports.keyboardTag = keyboardTag;
