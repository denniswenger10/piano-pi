"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.fretsTag = void 0;
var _option = require("../../../option");
var _common = require("../../../common");
const fretsTag = (name)=>{
    return _common.textIncludesAny([
        'guitar',
        'banjo',
        'ukulele',
        'mandol',
        'clavinet',
        'celesta',
        'harpsichord',
        'honky tonk',
        'honky-tonk',
        'bass', 
    ], name) && !_common.textIncludesAny([
        'synth',
        'bassoon',
        'strings',
        'sax',
        'tuba',
        'contra'
    ], name) ? _option.some('frets') : _option.none();
};
exports.fretsTag = fretsTag;
