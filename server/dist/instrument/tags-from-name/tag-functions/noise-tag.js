"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.noiseTag = void 0;
var _option = require("../../../option");
var _common = require("../../../common");
const noiseTag = (name)=>{
    return _common.textIncludesAny([
        'bird',
        'telephone',
        'gun',
        'applause',
        'noise',
        'helicopter',
        'sea shore', 
    ], name) && !_common.textIncludesAny([
        'guitar',
        'synth'
    ], name) ? _option.some('noise') : _option.none();
};
exports.noiseTag = noiseTag;
