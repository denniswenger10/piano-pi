"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.orchestraTag = void 0;
var _option = require("../../../option");
var _common = require("../../../common");
const orchestraTag = (name)=>{
    return _common.textIncludesAny([
        'viol',
        'string',
        'orchest',
        'flute',
        'horn',
        'trombone',
        'tuba',
        'trumpet',
        'brass',
        'pizzicato',
        'cello',
        'contra',
        'oboe',
        'clarinet',
        'piccolo', 
    ], name) && !_common.textIncludesAny([
        'guitar',
        'synth'
    ], name) ? _option.some('orchestra') : _option.none();
};
exports.orchestraTag = orchestraTag;
