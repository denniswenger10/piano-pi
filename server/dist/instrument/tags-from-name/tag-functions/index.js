"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
var _fretsTag = require("./frets-tag");
var _keyboardTag = require("./keyboard-tag");
var _orchestraTag = require("./orchestra-tag");
var _noiseTag = require("./noise-tag");
function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
        return obj;
    } else {
        var newObj = {
        };
        if (obj != null) {
            for(var key in obj){
                if (Object.prototype.hasOwnProperty.call(obj, key)) {
                    var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {
                    };
                    if (desc.get || desc.set) {
                        Object.defineProperty(newObj, key, desc);
                    } else {
                        newObj[key] = obj[key];
                    }
                }
            }
        }
        newObj.default = obj;
        return newObj;
    }
}
Object.keys(_fretsTag).forEach(function(key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function() {
            return _fretsTag[key];
        }
    });
});
Object.keys(_keyboardTag).forEach(function(key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function() {
            return _keyboardTag[key];
        }
    });
});
Object.keys(_orchestraTag).forEach(function(key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function() {
            return _orchestraTag[key];
        }
    });
});
Object.keys(_noiseTag).forEach(function(key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function() {
            return _noiseTag[key];
        }
    });
});
