"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.instrumentList = void 0;
const instrumentList = [
    {
        name: 'Yamaha Grand Piano',
        bank: 0,
        tags: [],
        number: 0,
        id: '0a3a8386-0f3d-4da5-9b2d-c9b9368f961b'
    },
    {
        name: 'Bright Yamaha Grand',
        bank: 0,
        tags: [],
        number: 1,
        id: '70fb54e7-a615-4e25-baf6-ba01b7d0a38d'
    },
    {
        name: 'Electric Piano',
        bank: 0,
        tags: [],
        number: 2,
        id: '7c99c44d-9e5c-49fb-8246-893db77fc260'
    },
    {
        name: 'Honky Tonk',
        bank: 0,
        tags: [],
        number: 3,
        id: '58e31804-c24b-42b5-a0ec-b948d28fb86c'
    },
    {
        name: 'Rhodes EP',
        bank: 0,
        tags: [],
        number: 4,
        id: '515b9e91-7624-4709-9f07-c7a91f85d21a'
    },
    {
        name: 'Legend EP 2',
        bank: 0,
        tags: [],
        number: 5,
        id: '5d4f8740-3ef6-4ce8-92af-920368bc8c88'
    },
    {
        name: 'Harpsichord',
        bank: 0,
        tags: [],
        number: 6,
        id: '9eb8ab67-4187-4bc4-b28d-ff17d6aa69ee'
    },
    {
        name: 'Clavinet',
        bank: 0,
        tags: [],
        number: 7,
        id: 'ec41d084-aee1-4e62-b45d-ae56659b3c1d'
    },
    {
        name: 'Celesta',
        bank: 0,
        tags: [],
        number: 8,
        id: 'b8e6a019-379d-4e58-9891-5c8e82378eae'
    },
    {
        name: 'Glockenspiel',
        bank: 0,
        tags: [],
        number: 9,
        id: '0ac07c77-8d91-434b-99ec-38d47abf3dc1'
    },
    {
        name: 'Music Box',
        bank: 0,
        tags: [],
        number: 10,
        id: '2cef82d2-e887-4522-afb0-e1bcbb576cd1'
    },
    {
        name: 'Vibraphone',
        bank: 0,
        tags: [],
        number: 11,
        id: '628aa340-5a32-4ade-8c1d-da7ddcfe1582'
    },
    {
        name: 'Marimba',
        bank: 0,
        tags: [],
        number: 12,
        id: 'd7954dd3-4dbc-4d45-adb6-ebc26529d14f'
    },
    {
        name: 'Xylophone',
        bank: 0,
        tags: [],
        number: 13,
        id: 'fbbd4257-3c1c-4356-8005-5cb311419d5f'
    },
    {
        name: 'Tubular Bells',
        bank: 0,
        tags: [],
        number: 14,
        id: 'a32097f1-016b-4ab7-b8af-9a465b462da0'
    },
    {
        name: 'Dulcimer',
        bank: 0,
        tags: [],
        number: 15,
        id: '508ee982-07b6-4e33-b8e3-4f7796a0dff7'
    },
    {
        name: 'DrawbarOrgan',
        bank: 0,
        tags: [],
        number: 16,
        id: 'c7b7f62e-4e2f-4fe6-a0aa-669bf060e2cc'
    },
    {
        name: 'Percussive Organ',
        bank: 0,
        tags: [],
        number: 17,
        id: '62fc9755-5249-4fcb-87e5-9eec093106e0'
    },
    {
        name: 'Rock Organ',
        bank: 0,
        tags: [],
        number: 18,
        id: '3f62a3b8-e91f-48d6-98ff-bf788d90b512'
    },
    {
        name: 'Church Organ',
        bank: 0,
        tags: [],
        number: 19,
        id: '88637070-e060-4fc2-97e5-bd223a30178a'
    },
    {
        name: 'Reed Organ',
        bank: 0,
        tags: [],
        number: 20,
        id: 'fd36711d-f196-41b3-ade4-bce85cd76831'
    },
    {
        name: 'Accordian',
        bank: 0,
        tags: [],
        number: 21,
        id: '32b78564-0c70-4bc2-9408-6204b830f06e'
    },
    {
        name: 'Harmonica',
        bank: 0,
        tags: [],
        number: 22,
        id: '79216c3c-7524-4562-a35b-7b6180916b0b'
    },
    {
        name: 'Bandoneon',
        bank: 0,
        tags: [],
        number: 23,
        id: '22fca42d-bd70-4904-8286-2519f933101d'
    },
    {
        name: 'Nylon String Guitar',
        bank: 0,
        tags: [],
        number: 24,
        id: '9e04bea5-c6f9-4469-9bd5-a21604832780'
    },
    {
        name: 'Steel String Guitar',
        bank: 0,
        tags: [],
        number: 25,
        id: '19d17fcd-8bca-4d46-a233-7371656579af'
    },
    {
        name: 'Jazz Guitar',
        bank: 0,
        tags: [],
        number: 26,
        id: '74d06f3b-8480-44c0-92c2-88f6363baf65'
    },
    {
        name: 'Clean Guitar',
        bank: 0,
        tags: [],
        number: 27,
        id: '79c82844-fe0c-415a-8754-05d93e87f9a1'
    },
    {
        name: 'Palm Muted Guitar',
        bank: 0,
        tags: [],
        number: 28,
        id: '97121f57-2d8f-41a7-aa30-1e6895e565ad'
    },
    {
        name: 'Overdrive Guitar',
        bank: 0,
        tags: [],
        number: 29,
        id: '353c03af-0a96-43c9-8f2e-b6aa7b278a83'
    },
    {
        name: 'Distortion Guitar',
        bank: 0,
        tags: [],
        number: 30,
        id: '2e023a03-0134-4b8d-bef6-ad64333c9eaa'
    },
    {
        name: 'Guitar Harmonics',
        bank: 0,
        tags: [],
        number: 31,
        id: 'cc377277-23c0-4251-a181-32bfef83e7e2'
    },
    {
        name: 'Acoustic Bass',
        bank: 0,
        tags: [],
        number: 32,
        id: 'f97eca9f-1d7b-4041-a632-8ce87c6e7ab3'
    },
    {
        name: 'Fingered Bass',
        bank: 0,
        tags: [],
        number: 33,
        id: 'c80f0bae-c16c-4daa-9074-b7b31a9152c1'
    },
    {
        name: 'Picked Bass',
        bank: 0,
        tags: [],
        number: 34,
        id: '19454834-719e-4b40-a133-8b30d84edf10'
    },
    {
        name: 'Fretless Bass',
        bank: 0,
        tags: [],
        number: 35,
        id: '61c87639-186a-4675-ad10-fc10866bbef6'
    },
    {
        name: 'Slap Bass',
        bank: 0,
        tags: [],
        number: 36,
        id: '19556114-076d-4d1a-9e45-1f085d60779f'
    },
    {
        name: 'Pop Bass',
        bank: 0,
        tags: [],
        number: 37,
        id: 'b54f3b18-991a-4861-a048-43e52daceefb'
    },
    {
        name: 'Synth Bass 1',
        bank: 0,
        tags: [],
        number: 38,
        id: '8de6135b-8678-49ac-ac5e-99acadfab33c'
    },
    {
        name: 'Synth Bass 2',
        bank: 0,
        tags: [],
        number: 39,
        id: '24a0dca8-9ee9-48cb-a7b3-ffb7426d68f8'
    },
    {
        name: 'Violin',
        bank: 0,
        tags: [],
        number: 40,
        id: '89aa4d44-afa6-4c15-af77-47638700d610'
    },
    {
        name: 'Viola',
        bank: 0,
        tags: [],
        number: 41,
        id: 'a1cc7381-310b-4fe0-a7b0-d7d2dc121fe0'
    },
    {
        name: 'Cello',
        bank: 0,
        tags: [],
        number: 42,
        id: '58d88011-cb34-4481-b84c-2a892a285122'
    },
    {
        name: 'Contrabass',
        bank: 0,
        tags: [],
        number: 43,
        id: '51695a53-c179-4089-ade8-bd757009efb5'
    },
    {
        name: 'Tremolo',
        bank: 0,
        tags: [],
        number: 44,
        id: '07d6bcc4-9de7-4d13-8e3a-e0afc5fabfb5'
    },
    {
        name: 'Pizzicato Section',
        bank: 0,
        tags: [],
        number: 45,
        id: '0afae9e9-471f-43ba-b3e7-54df1fdfbe05'
    },
    {
        name: 'Harp',
        bank: 0,
        tags: [],
        number: 46,
        id: '1db5a63e-8465-46de-9cc0-83a76e759a03'
    },
    {
        name: 'Timpani',
        bank: 0,
        tags: [],
        number: 47,
        id: 'cf19f1fb-d68a-4ad0-afe7-8048d13552d1'
    },
    {
        name: 'Strings',
        bank: 0,
        tags: [],
        number: 48,
        id: '7b53a182-b8c1-4d25-9e28-4424b2a04330'
    },
    {
        name: 'Slow Strings',
        bank: 0,
        tags: [],
        number: 49,
        id: '6a5a3e6a-f3fd-4052-a95a-a9cf5ab3537c'
    },
    {
        name: 'Synth Strings 1',
        bank: 0,
        tags: [],
        number: 50,
        id: '64d564ab-5d63-414f-a8c5-be424788c9de'
    },
    {
        name: 'Synth Strings 2',
        bank: 0,
        tags: [],
        number: 51,
        id: 'e1d86e24-b8e0-4d86-a2fc-b787b09caec3'
    },
    {
        name: 'Ahh Choir',
        bank: 0,
        tags: [],
        number: 52,
        id: 'b65a0b5a-4787-436c-b7d0-e8207e922de1'
    },
    {
        name: 'Ohh Voices',
        bank: 0,
        tags: [],
        number: 53,
        id: 'c6dd0895-635b-497f-9bef-18799dfe88a2'
    },
    {
        name: 'Synth Voice',
        bank: 0,
        tags: [],
        number: 54,
        id: 'a5da87d6-a9d6-4683-b5ef-3d392ad4d6a7'
    },
    {
        name: 'Orchestra Hit',
        bank: 0,
        tags: [],
        number: 55,
        id: '981bc013-9f36-4fb8-bd15-a755099b0ee7'
    },
    {
        name: 'Trumpet',
        bank: 0,
        tags: [],
        number: 56,
        id: '1669f509-6cd8-4729-b0c1-3eea4ec07ee9'
    },
    {
        name: 'Trombone',
        bank: 0,
        tags: [],
        number: 57,
        id: '5ac4b03f-e3bc-40ca-b6c7-9aafc250cf3c'
    },
    {
        name: 'Tuba',
        bank: 0,
        tags: [],
        number: 58,
        id: '8c1975ee-c526-4861-902f-35a0a8c85faf'
    },
    {
        name: 'Muted Trumpet',
        bank: 0,
        tags: [],
        number: 59,
        id: 'c2615d6d-b908-475c-b66f-d516f13d336e'
    },
    {
        name: 'French Horns',
        bank: 0,
        tags: [],
        number: 60,
        id: '8eb0cb9f-a361-4243-a23d-e66fe9c1294c'
    },
    {
        name: 'Brass Section',
        bank: 0,
        tags: [],
        number: 61,
        id: '522d3784-a146-4458-ae0e-89d0e508f373'
    },
    {
        name: 'Synth Brass 1',
        bank: 0,
        tags: [],
        number: 62,
        id: '440dbadb-0a5b-49c1-8367-b330f154e855'
    },
    {
        name: 'Synth Brass 2',
        bank: 0,
        tags: [],
        number: 63,
        id: 'e5d89034-6666-45bd-ba61-12cced1b95e6'
    },
    {
        name: 'Soprano Sax',
        bank: 0,
        tags: [],
        number: 64,
        id: '6de1b654-b64e-4310-a25c-e74432a51f6e'
    },
    {
        name: 'Alto Sax',
        bank: 0,
        tags: [],
        number: 65,
        id: '3e49cf79-c493-48ad-a4fd-ed2d234c257e'
    },
    {
        name: 'Tenor Sax',
        bank: 0,
        tags: [],
        number: 66,
        id: 'f53d6cda-8562-4ac2-9b1d-ec8b74e57ca6'
    },
    {
        name: 'Baritone Sax',
        bank: 0,
        tags: [],
        number: 67,
        id: '1bb315e5-95ed-41f9-9b46-4862d0c74e5b'
    },
    {
        name: 'Oboe',
        bank: 0,
        tags: [],
        number: 68,
        id: '748c7040-b348-4f08-a4cb-e89c0811a6a0'
    },
    {
        name: 'English Horn',
        bank: 0,
        tags: [],
        number: 69,
        id: 'e665250a-685b-4d72-8295-b2a561d2b040'
    },
    {
        name: 'Bassoon',
        bank: 0,
        tags: [],
        number: 70,
        id: 'd5b6d6ed-9df5-4103-92dc-b36138bc2409'
    },
    {
        name: 'Clarinet',
        bank: 0,
        tags: [],
        number: 71,
        id: 'd9b785ec-ad86-4201-882d-ed33f745ade6'
    },
    {
        name: 'Piccolo',
        bank: 0,
        tags: [],
        number: 72,
        id: '5504cfe4-f1f7-4762-b7f2-42b528777da0'
    },
    {
        name: 'Flute',
        bank: 0,
        tags: [],
        number: 73,
        id: '31b7b293-808a-4170-978a-35cd6c01d1dc'
    },
    {
        name: 'Recorder',
        bank: 0,
        tags: [],
        number: 74,
        id: '4f82b683-5b25-4ae3-864f-bcf4626cb543'
    },
    {
        name: 'Pan Flute',
        bank: 0,
        tags: [],
        number: 75,
        id: 'b2ffda6d-2f99-43cc-8043-69de4fd40567'
    },
    {
        name: 'Bottle Chiff',
        bank: 0,
        tags: [],
        number: 76,
        id: 'fa0a5c1a-92e1-4854-8b5e-91725a6e8e94'
    },
    {
        name: 'Shakuhachi',
        bank: 0,
        tags: [],
        number: 77,
        id: 'e0a7efab-73ab-4525-aa13-33cda5749360'
    },
    {
        name: 'Whistle',
        bank: 0,
        tags: [],
        number: 78,
        id: 'f95b0e0d-527d-4f73-b66f-a783cfa57d4d'
    },
    {
        name: 'Ocarina',
        bank: 0,
        tags: [],
        number: 79,
        id: 'a4cf7d26-aa9d-4e75-825a-72b7142cb53b'
    },
    {
        name: 'Square Lead',
        bank: 0,
        tags: [],
        number: 80,
        id: '23d49771-36e9-42c8-837b-7cb2c246efbb'
    },
    {
        name: 'Saw Wave',
        bank: 0,
        tags: [],
        number: 81,
        id: '0f8e67f3-a293-41a6-99f6-5270dca56040'
    },
    {
        name: 'Calliope Lead',
        bank: 0,
        tags: [],
        number: 82,
        id: 'f3b45390-9658-47fa-95a8-2c8e8398fa06'
    },
    {
        name: 'Chiffer Lead',
        bank: 0,
        tags: [],
        number: 83,
        id: 'f15894eb-39f5-4a99-bd30-daa15fd5b059'
    },
    {
        name: 'Charang',
        bank: 0,
        tags: [],
        number: 84,
        id: 'b5e08cba-43c3-43df-beba-46dfe79a741c'
    },
    {
        name: 'Solo Vox',
        bank: 0,
        tags: [],
        number: 85,
        id: '84a42a93-b222-4aad-8645-3099e167716d'
    },
    {
        name: 'Fifth Sawtooth Wave',
        bank: 0,
        tags: [],
        number: 86,
        id: '3d3f7b9e-8c50-4197-88f3-7429dc955cb1'
    },
    {
        name: 'Bass & Lead',
        bank: 0,
        tags: [],
        number: 87,
        id: 'cc8b04c2-495b-4173-bf20-0308f640274e'
    },
    {
        name: 'Fantasia',
        bank: 0,
        tags: [],
        number: 88,
        id: 'aec09a09-9ea4-4fb8-b8b6-cdf42d7bc6e5'
    },
    {
        name: 'Warm Pad',
        bank: 0,
        tags: [],
        number: 89,
        id: '8f8992ff-7ac7-4949-96c8-3a9218fc0c99'
    },
    {
        name: 'Polysynth',
        bank: 0,
        tags: [],
        number: 90,
        id: '97d9b5f6-25e4-4bf7-b24e-f910db668987'
    },
    {
        name: 'Space Voice',
        bank: 0,
        tags: [],
        number: 91,
        id: 'e654efb8-0615-4281-ac49-827d98aff82b'
    },
    {
        name: 'Bowed Glass',
        bank: 0,
        tags: [],
        number: 92,
        id: '891a38de-7ede-47c4-89b1-9321b1640b4a'
    },
    {
        name: 'Metal Pad',
        bank: 0,
        tags: [],
        number: 93,
        id: 'b7a33eee-164c-42b9-9279-b462532f8423'
    },
    {
        name: 'Halo Pad',
        bank: 0,
        tags: [],
        number: 94,
        id: '7ca76a08-9f30-45a0-bf08-c5b5aed6cee0'
    },
    {
        name: 'Sweep Pad',
        bank: 0,
        tags: [],
        number: 95,
        id: 'be552ea9-1e71-400d-be39-7dbeda89ac51'
    },
    {
        name: 'Ice Rain',
        bank: 0,
        tags: [],
        number: 96,
        id: '729b5b6c-8718-431a-a423-ee33d12978c3'
    },
    {
        name: 'Soundtrack',
        bank: 0,
        tags: [],
        number: 97,
        id: 'b7d896ff-1489-41a8-ae09-a19b98bb075a'
    },
    {
        name: 'Crystal',
        bank: 0,
        tags: [],
        number: 98,
        id: 'd9074f91-2b3d-4f54-bca3-b75eeb403164'
    },
    {
        name: 'Atmosphere',
        bank: 0,
        tags: [],
        number: 99,
        id: '06dd405f-07b4-407b-acf4-d4ac9df5fa75'
    },
    {
        name: 'Brightness',
        bank: 0,
        tags: [],
        number: 100,
        id: '71a0381f-28a6-49b2-b366-91777ded8bf6'
    },
    {
        name: 'Goblin',
        bank: 0,
        tags: [],
        number: 101,
        id: '16258ad4-765e-49e0-8f4e-5f9bbfc10ef7'
    },
    {
        name: 'Echo Drops',
        bank: 0,
        tags: [],
        number: 102,
        id: 'faeb1961-b89b-4ee1-ac93-45b3a6f9c350'
    },
    {
        name: 'Star Theme',
        bank: 0,
        tags: [],
        number: 103,
        id: '2dd39ff6-9c3b-41a1-a2a6-bf98e6fe0468'
    },
    {
        name: 'Sitar',
        bank: 0,
        tags: [],
        number: 104,
        id: 'a9cc006a-6fc6-4afa-b66b-566f895511f4'
    },
    {
        name: 'Banjo',
        bank: 0,
        tags: [],
        number: 105,
        id: '4611bb58-265b-4ca1-bacc-6fe6e33fbfab'
    },
    {
        name: 'Shamisen',
        bank: 0,
        tags: [],
        number: 106,
        id: '63e6d8a5-18ff-425e-abd5-e234eb1f2582'
    },
    {
        name: 'Koto',
        bank: 0,
        tags: [],
        number: 107,
        id: '98844b8a-4f51-4266-b0ad-590a97e67b0b'
    },
    {
        name: 'Kalimba',
        bank: 0,
        tags: [],
        number: 108,
        id: '6ae29b14-9ec6-40e3-80b7-08c66165b4d7'
    },
    {
        name: 'BagPipe',
        bank: 0,
        tags: [],
        number: 109,
        id: 'f333d11d-9485-49c9-8df0-64bc26cdbd24'
    },
    {
        name: 'Fiddle',
        bank: 0,
        tags: [],
        number: 110,
        id: 'bccf43fc-b716-4e27-b8a4-418945834fdb'
    },
    {
        name: 'Shenai',
        bank: 0,
        tags: [],
        number: 111,
        id: '4b511a1e-47a7-4743-bf28-bf0ba561e273'
    },
    {
        name: 'Tinker Bell',
        bank: 0,
        tags: [],
        number: 112,
        id: '8f7c9c5a-4629-426e-8fc0-b17f2e38d0cc'
    },
    {
        name: 'Agogo',
        bank: 0,
        tags: [],
        number: 113,
        id: 'd6c794ab-6c8a-46e3-bb83-5e5dc795e421'
    },
    {
        name: 'Steel Drums',
        bank: 0,
        tags: [],
        number: 114,
        id: 'c423e805-6ddc-4962-bfcc-c1f8f322a735'
    },
    {
        name: 'Woodblock',
        bank: 0,
        tags: [],
        number: 115,
        id: 'db1139f5-80e3-4eff-8936-713dc23cf095'
    },
    {
        name: 'Taiko Drum',
        bank: 0,
        tags: [],
        number: 116,
        id: '0582b3cd-174f-4854-be0d-9c0cfcea80c6'
    },
    {
        name: 'Melodic Tom',
        bank: 0,
        tags: [],
        number: 117,
        id: '2ca28a71-456b-4910-80bb-62a16c29d97c'
    },
    {
        name: 'Synth Drum',
        bank: 0,
        tags: [],
        number: 118,
        id: '518a7738-92b0-4215-bbe5-5c7cc2f64338'
    },
    {
        name: 'Reverse Cymbal',
        bank: 0,
        tags: [],
        number: 119,
        id: 'd921e84d-79e2-49bd-b241-e1d1be1d72ec'
    },
    {
        name: 'Fret Noise',
        bank: 0,
        tags: [],
        number: 120,
        id: '2dc38ee9-f7ee-4112-af64-5665ddc6c74c'
    },
    {
        name: 'Breath Noise',
        bank: 0,
        tags: [],
        number: 121,
        id: '5a70ffa2-1755-4a4d-a5ce-20e741982baf'
    },
    {
        name: 'Sea Shore',
        bank: 0,
        tags: [],
        number: 122,
        id: 'dcb8074c-f79b-42f1-af7d-beabc8cee4e4'
    },
    {
        name: 'Bird Tweet',
        bank: 0,
        tags: [],
        number: 123,
        id: '3f605f42-5dc6-4c27-9c13-f7bd1ed737f4'
    },
    {
        name: 'Telephone',
        bank: 0,
        tags: [],
        number: 124,
        id: '9f8e9bc8-28af-4b3f-8d1c-3b3772734dde'
    },
    {
        name: 'Helicopter',
        bank: 0,
        tags: [],
        number: 125,
        id: 'bc03ed96-2f90-4414-85e0-2d0865a86d47'
    },
    {
        name: 'Applause',
        bank: 0,
        tags: [],
        number: 126,
        id: '38c4a5fc-404a-4aa3-af41-0ff7c1ee7158'
    },
    {
        name: 'Gun Shot',
        bank: 0,
        tags: [],
        number: 127,
        id: 'e87fb99a-cb40-4ef7-af23-bf76b12d5e03'
    },
    {
        name: 'Detuned EP 1',
        bank: 8,
        tags: [],
        number: 4,
        id: 'a1e2905d-7b0f-4910-ac8b-e63e990491a9'
    },
    {
        name: 'Detuned EP 2',
        bank: 8,
        tags: [],
        number: 5,
        id: '2ac37afa-a7dd-4a19-9e93-7472e596e82d'
    },
    {
        name: 'Coupled Harpsichord',
        bank: 8,
        tags: [],
        number: 6,
        id: '434cfbad-1816-4ea5-b3a7-ca20d1429667'
    },
    {
        name: 'Church Bell',
        bank: 8,
        tags: [],
        number: 14,
        id: 'eb61fc58-f771-4c29-b0f7-320f592424a7'
    },
    {
        name: 'Detuned Organ 1',
        bank: 8,
        tags: [],
        number: 16,
        id: 'e893f87d-8a4f-405a-bfb5-0270dccb91ca'
    },
    {
        name: 'Detuned Organ 2',
        bank: 8,
        tags: [],
        number: 17,
        id: 'ca96ef60-4c1b-40a0-9610-9e07c7b90291'
    },
    {
        name: 'Church Organ 2',
        bank: 8,
        tags: [],
        number: 19,
        id: '2137cd05-782b-44ec-8630-0a1d7a1f56a3'
    },
    {
        name: 'Italian Accordion',
        bank: 8,
        tags: [],
        number: 21,
        id: 'b50d03bb-f743-43b6-ba29-580833cab9c4'
    },
    {
        name: 'Ukulele',
        bank: 8,
        tags: [],
        number: 24,
        id: 'ad24f952-55a0-435e-b2b6-d1adfa29ae25'
    },
    {
        name: '12 String Guitar',
        bank: 8,
        tags: [],
        number: 25,
        id: 'ed0042ef-02b4-4271-8643-b0fe074c6b39'
    },
    {
        name: 'Hawaiian Guitar',
        bank: 8,
        tags: [],
        number: 26,
        id: '5d48fbc9-1881-4a76-bcd1-d09265a9547a'
    },
    {
        name: 'Funk Guitar',
        bank: 8,
        tags: [],
        number: 28,
        id: '36e8109b-8e85-44ae-a99b-d216a4af88f6'
    },
    {
        name: 'Feedback Guitar',
        bank: 8,
        tags: [],
        number: 30,
        id: '952263a1-eec5-4d89-867f-c270229a748e'
    },
    {
        name: 'Guitar Feedback',
        bank: 8,
        tags: [],
        number: 31,
        id: '493bddc2-6388-4bbc-859b-209423c639b7'
    },
    {
        name: 'Synth Bass 3',
        bank: 8,
        tags: [],
        number: 38,
        id: '24aec0e0-2f19-49df-9803-403ff40d7105'
    },
    {
        name: 'Synth Bass 4',
        bank: 8,
        tags: [],
        number: 39,
        id: '6f5b1665-eb47-4a08-8677-12b50d9f2e0d'
    },
    {
        name: 'Slow Violin',
        bank: 8,
        tags: [],
        number: 40,
        id: 'fbd8bada-edd0-418a-b8fc-699224f6152e'
    },
    {
        name: 'Orchestral Pad',
        bank: 8,
        tags: [],
        number: 48,
        id: '311a6b08-37af-4d05-9b3d-10bdd919ed64'
    },
    {
        name: 'Synth Strings 3',
        bank: 8,
        tags: [],
        number: 50,
        id: 'a1df7c69-b112-4450-b864-8e5eb6644a35'
    },
    {
        name: 'Brass 2',
        bank: 8,
        tags: [],
        number: 61,
        id: '00e586f1-8bec-4acc-b980-da90c2ec85c3'
    },
    {
        name: 'Synth Brass 3',
        bank: 8,
        tags: [],
        number: 62,
        id: '55bfd238-889c-4321-aee3-cf13837ee6bc'
    },
    {
        name: 'Synth Brass 4',
        bank: 8,
        tags: [],
        number: 63,
        id: 'df6dfa64-ee61-491f-a9ca-1485401124fd'
    },
    {
        name: 'Sine Wave',
        bank: 8,
        tags: [],
        number: 80,
        id: 'a365cb5d-c238-4c1a-b927-d4634ce5cbde'
    },
    {
        name: 'Taisho Koto',
        bank: 8,
        tags: [],
        number: 107,
        id: '523fe128-f51c-4834-ad75-469f397c92c7'
    },
    {
        name: 'Castanets',
        bank: 8,
        tags: [],
        number: 115,
        id: 'c12bdd85-1479-4f76-80c8-e0824e6698bd'
    },
    {
        name: 'Concert Bass Drum',
        bank: 8,
        tags: [],
        number: 116,
        id: 'ba9b10f6-e095-4e5a-9075-ef0345ef27d8'
    },
    {
        name: 'Melo Tom 2',
        bank: 8,
        tags: [],
        number: 117,
        id: '71124bcd-a2e7-4507-bb40-983cac049e96'
    },
    {
        name: '808 Tom',
        bank: 8,
        tags: [],
        number: 118,
        id: '60e7eeb6-ad5a-47e1-a9e2-6c20477067a1'
    },
    {
        name: 'Burst Noise',
        bank: 9,
        tags: [],
        number: 125,
        id: 'b1bccb0f-7b8d-4a0b-ab82-dcaa134f93aa'
    },
    {
        name: 'Mandolin',
        bank: 16,
        tags: [],
        number: 25,
        id: 'ce561e47-f656-409d-8aa8-2639cc47c0f0'
    },
    {
        name: 'Standard',
        bank: 128,
        tags: [],
        number: 0,
        id: 'b4c68152-23c8-40cc-8edc-18af3ecb4afe'
    },
    {
        name: 'Standard 1',
        bank: 128,
        tags: [],
        number: 1,
        id: '2261bcff-6334-41eb-9828-a94b46d81547'
    },
    {
        name: 'Standard 2',
        bank: 128,
        tags: [],
        number: 2,
        id: '588b5d41-a8b1-4899-a614-942a7afde7df'
    },
    {
        name: 'Standard 3',
        bank: 128,
        tags: [],
        number: 3,
        id: '30504374-6b7f-4638-bb3f-470c33556867'
    },
    {
        name: 'Standard 4',
        bank: 128,
        tags: [],
        number: 4,
        id: '3b76ea34-e9e6-43b4-9563-3d204ddbd180'
    },
    {
        name: 'Standard 5',
        bank: 128,
        tags: [],
        number: 5,
        id: '44b39b10-b7a4-4b00-9b60-8cd1d2f82588'
    },
    {
        name: 'Standard 6',
        bank: 128,
        tags: [],
        number: 6,
        id: '7063782a-d7f9-41d3-9119-eee29076b15d'
    },
    {
        name: 'Standard 7',
        bank: 128,
        tags: [],
        number: 7,
        id: '39846b69-5475-48ef-a445-ff49459c2836'
    },
    {
        name: 'Room',
        bank: 128,
        tags: [],
        number: 8,
        id: '58fa414e-4e6e-44db-87ca-9510fd442c74'
    },
    {
        name: 'Room 1',
        bank: 128,
        tags: [],
        number: 9,
        id: '21040364-4a52-487b-90ae-e90eb49c4c07'
    },
    {
        name: 'Room 2',
        bank: 128,
        tags: [],
        number: 10,
        id: 'ad23127d-7eed-4d90-8524-9d0d08e0acf7'
    },
    {
        name: 'Room 3',
        bank: 128,
        tags: [],
        number: 11,
        id: '37e653d5-0c31-4b93-9cc1-caac32348f45'
    },
    {
        name: 'Room 4',
        bank: 128,
        tags: [],
        number: 12,
        id: '0293ad49-b4b0-49ed-a00e-d491c5832167'
    },
    {
        name: 'Room 5',
        bank: 128,
        tags: [],
        number: 13,
        id: '83cde124-5d43-415b-8f9c-372a7d2ccfac'
    },
    {
        name: 'Room 6',
        bank: 128,
        tags: [],
        number: 14,
        id: '773b62b5-2504-413f-8d1f-6b69de67bd39'
    },
    {
        name: 'Room 7',
        bank: 128,
        tags: [],
        number: 15,
        id: 'b37746d0-7ec9-4891-a187-3430d1697eda'
    },
    {
        name: 'Power',
        bank: 128,
        tags: [],
        number: 16,
        id: '6222ddeb-16d7-4255-bf7b-377f5b9e5f65'
    },
    {
        name: 'Power 1',
        bank: 128,
        tags: [],
        number: 17,
        id: '3f5970bc-83ea-4188-9214-8bb3bd2ae40d'
    },
    {
        name: 'Power 2',
        bank: 128,
        tags: [],
        number: 18,
        id: 'c9c12d35-f4eb-4f7d-bb0e-e1d429d01ab4'
    },
    {
        name: 'Power 3',
        bank: 128,
        tags: [],
        number: 19,
        id: 'beb7cfca-476d-410d-a58c-792d13b643ce'
    },
    {
        name: 'Electronic',
        bank: 128,
        tags: [],
        number: 24,
        id: 'ebf9ded0-34ed-4a0c-a5d8-c09f582c5f51'
    },
    {
        name: 'TR-808',
        bank: 128,
        tags: [],
        number: 25,
        id: '6688ef73-50d0-4828-9824-4134ec0285de'
    },
    {
        name: 'Jazz',
        bank: 128,
        tags: [],
        number: 32,
        id: 'fd4e4c61-82a4-482e-92e2-3a0ec08d47da'
    },
    {
        name: 'Jazz 1',
        bank: 128,
        tags: [],
        number: 33,
        id: '190ae6ae-4206-4124-b332-9491fbf67639'
    },
    {
        name: 'Jazz 2',
        bank: 128,
        tags: [],
        number: 34,
        id: 'f7f9b9cc-aea0-4ebe-91dd-751fa903417f'
    },
    {
        name: 'Jazz 3',
        bank: 128,
        tags: [],
        number: 35,
        id: '591922f7-fb80-4b73-8018-1ff336136963'
    },
    {
        name: 'Jazz 4',
        bank: 128,
        tags: [],
        number: 36,
        id: 'f7df4dc6-0f79-47e8-9a83-3beb7acbb0c9'
    },
    {
        name: 'Brush',
        bank: 128,
        tags: [],
        number: 40,
        id: '7eccb52d-a073-4e9d-a93e-59de96b87478'
    },
    {
        name: 'Brush 1',
        bank: 128,
        tags: [],
        number: 41,
        id: '7135bcd1-db2f-4da8-b232-bc48f1d502b4'
    },
    {
        name: 'Brush 2',
        bank: 128,
        tags: [],
        number: 42,
        id: '747ede16-24fc-4a10-926b-d3f516ee361e'
    },
    {
        name: 'Orchestra Kit',
        bank: 128,
        tags: [],
        number: 48,
        id: '21b2b2c2-a4d7-4cca-9965-74e046a1708c'
    }, 
];
exports.instrumentList = instrumentList;
