"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getAll = void 0;
var _instrumentState = require("./instrument-state");
var _piano = require("../piano");
const getAll = async ()=>{
    const cacheList = _instrumentState.instrumentState.instrumentList.read();
    if (cacheList.length < 1) {
        const connection = await _piano.fluidsynth();
        const fonts = await connection.fonts();
        const instruments = await connection.instrumentsFromFont(fonts[0].id);
        _instrumentState.instrumentState.instrumentList.update(instruments);
    }
    return _instrumentState.instrumentState.instrumentList.read();
};
exports.getAll = getAll;
