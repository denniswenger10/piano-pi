"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.none = void 0;
var _addBoolMethods = require("./add-bool-methods");
const none = ()=>{
    return {
        unwrap: ()=>{
            throw new Error("No data here. Don't use unwrap unless you know it's completely safe.");
        },
        unwrapOr: (or)=>or
        ,
        unwrapOrElse: (orElse)=>orElse()
        ,
        match: ({ none: _none  })=>_none()
        ,
        map: (_)=>none()
        ,
        mapOr: (_, or)=>or
        ,
        mapOrElse: (_, orElse)=>orElse()
        ,
        filter: ()=>none()
        ,
        zip: (_)=>none()
        ,
        // andThen: <T>(_: (d: DataType) => Option<T>) => none<T>(),
        ..._addBoolMethods.addBoolMethods(false)
    };
};
exports.none = none;
