"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.some = void 0;
var _addBoolMethods = require("./add-bool-methods");
var _none = require("./none");
const some = (data)=>{
    return {
        unwrap: ()=>data
        ,
        unwrapOr: ()=>data
        ,
        unwrapOrElse: ()=>data
        ,
        match: ({ some: _some  })=>_some(data)
        ,
        map: (fn)=>some(fn(data))
        ,
        mapOr: (fn)=>fn(data)
        ,
        mapOrElse: (fn)=>fn(data)
        ,
        filter: (fn)=>fn(data) ? some(data) : _none.none()
        ,
        zip: (secondData)=>secondData.map((x)=>[
                    data,
                    x
                ]
            )
        ,
        // andThen: (fn) => fn(data).map((_data) => _data),
        ..._addBoolMethods.addBoolMethods(true)
    };
};
exports.some = some;
