"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addBoolMethods = void 0;
const addBoolMethods = (isSome)=>({
        isSome: ()=>isSome
        ,
        isNone: ()=>!isSome
    })
;
exports.addBoolMethods = addBoolMethods;
