"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.filterMap = void 0;
const filterMap = (fn, list)=>{
    const returnList = [];
    for (const item of list){
        fn(item).match({
            some (data) {
                returnList.push(data);
            },
            none () {
            }
        });
    }
    return returnList;
};
exports.filterMap = filterMap;
