"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
var _option = require("./option");
var _none = require("./none");
var _some = require("./some");
var _filterMap = require("./filter-map");
function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
        return obj;
    } else {
        var newObj = {
        };
        if (obj != null) {
            for(var key in obj){
                if (Object.prototype.hasOwnProperty.call(obj, key)) {
                    var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {
                    };
                    if (desc.get || desc.set) {
                        Object.defineProperty(newObj, key, desc);
                    } else {
                        newObj[key] = obj[key];
                    }
                }
            }
        }
        newObj.default = obj;
        return newObj;
    }
}
Object.keys(_option).forEach(function(key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function() {
            return _option[key];
        }
    });
});
Object.keys(_none).forEach(function(key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function() {
            return _none[key];
        }
    });
});
Object.keys(_some).forEach(function(key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function() {
            return _some[key];
        }
    });
});
Object.keys(_filterMap).forEach(function(key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function() {
            return _filterMap[key];
        }
    });
});
