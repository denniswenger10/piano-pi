"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.startPiano = void 0;
var _childProcess = require("../child-process");
var _instrument = require("../instrument");
const startFluid = (soundfontPath, port)=>{
    return new Promise((resolve)=>{
        const fluidsynthProcess = _childProcess.spawn(`fluidsynth -s -a alsa -g 1 -c 2 -z 128 -r 48000 -o shell.port=${port} -m alsa_seq ${soundfontPath}`);
        fluidsynthProcess.stderr.on('data', (data)=>{
            console.error(`stderr: ${data}`);
        });
        fluidsynthProcess.on('close', ()=>{
            console.log('Fluidsynth closed');
        });
        fluidsynthProcess.on('exit', ()=>{
            console.log('Fluidsynth closed');
        });
        fluidsynthProcess.stdout.on('data', (data)=>{
            if (data.includes('>')) {
                resolve(fluidsynthProcess);
            }
        });
    });
};
const startPiano = async (soundfontPath = `~/downloads/fluid-soundfont-3.1/FluidR3_GM.sf2`, port = 9800)=>{
    const fluidsynthProcess = await startFluid(soundfontPath, port);
    // await delay(2000)
    console.log('Fluidsynth started');
    const { stderr  } = await _childProcess.exec('aconnect 20:0 128:0');
    if (stderr.length > 0) {
        console.log('This should be a problem');
    }
    console.log('Piano and fluidsynth connected');
    const instruments = await _instrument.getAll();
    if (instruments.length > 0) {
        await _instrument.selectInstrument(instruments[0].id);
    }
    return fluidsynthProcess;
};
exports.startPiano = startPiano;
