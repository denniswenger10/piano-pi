"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseFonts = void 0;
var _createSoundfont = require("./create-soundfont");
var _common = require("../../../common");
var _option = require("../../../option");
const parseRow = (row)=>{
    const [id, ...name] = row.trim().split(' ');
    if (isNaN(parseInt(id))) {
        return _option.none();
    }
    const soundfont = _createSoundfont.createSoundfont({
        id,
        name: name.join('')
    });
    return [
        soundfont.id.length > 0,
        soundfont.name.length > 0
    ].every((x)=>x
    ) ? _option.some(soundfont) : _option.none();
};
const parseFonts = (fontText)=>{
    const [, ...rows] = _common.stringToRows(fontText);
    return _option.filterMap(parseRow, rows);
};
exports.parseFonts = parseFonts;
