"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.fonts = void 0;
var _parseFonts = require("./parse-fonts");
const fonts = (connection)=>async ()=>{
        const resp = await connection.send('fonts');
        return _parseFonts.parseFonts(resp);
    }
;
exports.fonts = fonts;
