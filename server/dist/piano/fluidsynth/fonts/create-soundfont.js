"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.createSoundfont = void 0;
const createSoundfont = (soundfont = {
})=>{
    const { name ='' , id =''  } = soundfont;
    return {
        name,
        id
    };
};
exports.createSoundfont = createSoundfont;
