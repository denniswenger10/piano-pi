"use strict";
var _fsExtra = require("fs-extra");
var _path = _interopRequireDefault(require("path"));
var _option = require("../../../../option");
var _createSoundfontData = require("./create-soundfont-data");
function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
const fontDataDirPath = _path.default.join(__dirname, '../data');
const createPathToFile = (id)=>_path.default.join(fontDataDirPath, `${id}.json`)
;
const read = async (id)=>{
    const filePath = createPathToFile(id);
    return await _fsExtra.pathExists(filePath) ? _option.some(_createSoundfontData.createSoundfontData(await _fsExtra.readJson(filePath))) : _option.none();
};
const update = async ()=>{
};
const create = async (soundfontData)=>{
};
