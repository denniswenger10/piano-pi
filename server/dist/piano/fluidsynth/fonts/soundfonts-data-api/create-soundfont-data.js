"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.createSoundfontData = void 0;
var _instrument = require("../../../../instrument");
const createSoundfontData = (data)=>{
    const { name ='' , md5 ='' , instruments =[]  } = data;
    return {
        name,
        md5,
        instruments: instruments.map((x = {
        })=>_instrument.createInstrument(x)
        )
    };
};
exports.createSoundfontData = createSoundfontData;
