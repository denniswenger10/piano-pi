"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.selectInstrument = void 0;
var _common = require("../../../common");
const selectInstrument = (connection)=>async (instrument)=>{
        connection.send(`select 0 0 ${instrument.bank} ${instrument.number}`);
        await _common.delay(5);
        return '';
    }
;
exports.selectInstrument = selectInstrument;
