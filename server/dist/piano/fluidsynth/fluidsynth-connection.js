"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.fluidsynthConnection = void 0;
var _telnetClient = _interopRequireDefault(require("telnet-client"));
function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
let connection = null;
const createInterface = (connection1)=>{
    return {
        send: async (text)=>{
            const resp = await connection1.send(text);
            return resp.replace(/(?:\r\n> |\r> |\n> |^(> ))/g, '');
        },
        close: ()=>connection1.end()
    };
};
const fluidsynthConnection = async (port = 9800)=>{
    if (connection !== null) {
        return createInterface(connection);
    }
    connection = new _telnetClient.default();
    const params = {
        host: 'localhost',
        port,
        timeout: 1500,
        negotiationMandatory: false
    };
    await connection.connect(params);
    return createInterface(connection);
};
exports.fluidsynthConnection = fluidsynthConnection;
