"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getNameFromRow = void 0;
const getNameFromRow = (row)=>{
    const [, ...tail] = row.split(' ');
    return tail.join(' ');
};
exports.getNameFromRow = getNameFromRow;
