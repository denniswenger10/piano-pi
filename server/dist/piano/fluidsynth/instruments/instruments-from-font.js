"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.instrumentsFromFont = void 0;
var _parseInstruments = require("./parse-instruments");
const instrumentsFromFont = (connection)=>async (fontId)=>{
        const resp = await connection.send(`inst ${fontId}`);
        return _parseInstruments.parseInstruments(resp);
    }
;
exports.instrumentsFromFont = instrumentsFromFont;
