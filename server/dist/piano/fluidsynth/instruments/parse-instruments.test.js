"use strict";
var _parseInstruments = require("./parse-instruments");
describe('Parse fonts from fluidsynth', ()=>{
    it('should parse into object', ()=>{
        const data = `\n000-000 Yamaha Grand Piano\n000-001 Bright Yamaha Grand\n`;
        const result = _parseInstruments.parseInstruments(data).map(({ id , ...rest })=>rest
        );
        expect(result).toEqual([
            {
                name: 'Yamaha Grand Piano',
                number: 0,
                bank: 0,
                tags: []
            },
            {
                name: 'Bright Yamaha Grand',
                number: 1,
                bank: 0,
                tags: []
            }, 
        ]);
    });
});
