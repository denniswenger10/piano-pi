"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getBankFromRow = void 0;
var _common = require("../../../common");
const getBankFromRow = (row)=>+_common.firstOr('0')(row.split('-'))
;
exports.getBankFromRow = getBankFromRow;
