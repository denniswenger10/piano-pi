"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseInstruments = void 0;
var _common = require("../../../common");
var _instrument = require("../../../instrument");
var _option = require("../../../option");
var _getBankFromRow = require("./get-bank-from-row");
var _getNameFromRow = require("./get-name-from-row");
var _getNumberFromRow = require("./get-number-from-row");
var _getTagsFromRow = require("./get-tags-from-row");
const parseRow = (text)=>{
    const trimmed = text.trim();
    const bank = _getBankFromRow.getBankFromRow(trimmed);
    const number = _getNumberFromRow.getNumberFromRow(trimmed);
    const name = _getNameFromRow.getNameFromRow(trimmed);
    const tags = _getTagsFromRow.getTagsFromRow(name);
    const instrument = _instrument.createInstrument({
        bank,
        number,
        name,
        tags
    });
    return [
        instrument.name.length > 0
    ].every((x)=>x
    ) ? _option.some(instrument) : _option.none();
};
const parseInstruments = (text)=>{
    return _option.filterMap(parseRow, _common.stringToRows(text));
};
exports.parseInstruments = parseInstruments;
