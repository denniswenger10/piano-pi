"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getTagsFromRow = void 0;
var _instrument = require("../../../instrument");
const getTagsFromRow = (name)=>_instrument.tagsFromName(name)
;
exports.getTagsFromRow = getTagsFromRow;
