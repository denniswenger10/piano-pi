"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getNumberFromRow = void 0;
var _common = require("../../../common");
var ref;
const getNumberFromRow = (row)=>+((ref = _common.firstOr('0-0')(row.split(' ')).split('-')[1]) !== null && ref !== void 0 ? ref : '0')
;
exports.getNumberFromRow = getNumberFromRow;
