"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.fluidsynth = void 0;
var _fluidsynthConnection = require("./fluidsynth-connection");
var _fonts = require("./fonts");
var _instruments = require("./instruments");
var _selectInstrument = require("./select-instrument");
const fluidsynth = async (port = 9800)=>{
    const connection = await _fluidsynthConnection.fluidsynthConnection(port);
    return {
        fonts: _fonts.fonts(connection),
        instrumentsFromFont: _instruments.instrumentsFromFont(connection),
        selectInstrument: _selectInstrument.selectInstrument(connection)
    };
};
exports.fluidsynth = fluidsynth;
