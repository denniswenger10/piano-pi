"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.startPiano = void 0;
var _childProcess = require("../../child-process");
var _instrument = require("../../instrument");
var _startFluid = require("./start-fluid");
var _md5File = _interopRequireDefault(require("md5-file"));
function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
const startPiano = async (soundfontPaths = [
    `~/downloads/fluid-soundfont-3.1/FluidR3_GM.sf2`, 
], port = 9800)=>{
    const fluidsynthProcess = await _startFluid.startFluid(soundfontPaths, port);
    await _md5File.default(soundfontPaths[0]);
    console.log('Fluidsynth started');
    const { stderr  } = await _childProcess.exec('aconnect 20:0 128:0');
    if (stderr.length > 0) {
        console.log('This should be a problem');
    }
    console.log('Piano and fluidsynth connected');
    const instruments = await _instrument.getAll();
    if (instruments.length > 0) {
        await _instrument.selectInstrument(instruments[0].id);
    }
    return fluidsynthProcess;
};
exports.startPiano = startPiano;
