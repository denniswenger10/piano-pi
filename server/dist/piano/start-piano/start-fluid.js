"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.startFluid = void 0;
var _childProcess = require("../../child-process");
const startFluid = (soundfontPaths, port)=>{
    return new Promise((resolve, reject)=>{
        const fluidsynthProcess = _childProcess.spawn(`fluidsynth -s -a alsa -g 1 -c 2 -z 128 -r 48000 -o shell.port=${port} -m alsa_seq ${soundfontPaths.join(' ')}`);
        fluidsynthProcess.stderr.on('data', (data)=>{
            console.error(`fluidsynth: ${data}`);
            reject(`fluidsynth error: ${data}`);
        });
        fluidsynthProcess.on('close', ()=>{
            console.log('Fluidsynth closed');
        });
        fluidsynthProcess.on('exit', ()=>{
            console.log('Fluidsynth closed');
        });
        fluidsynthProcess.stdout.on('data', (data)=>{
            if (data.includes('>')) {
                resolve(fluidsynthProcess);
            }
        });
    });
};
exports.startFluid = startFluid;
