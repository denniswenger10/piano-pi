"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.instrumentRoutes = void 0;
var _instrument = require("../../instrument");
const instrumentRoutes = (instance, _, next)=>{
    instance.get('/', async ()=>{
        return {
            meta: {
            },
            data: await _instrument.getAll()
        };
    });
    instance.get('/select', async ()=>{
        return {
            meta: {
            },
            data: _instrument.instrumentState.selectedInstrument.read().unwrapOr(_instrument.createInstrument())
        };
    });
    instance.get('/select/:id', async (req)=>{
        const { id  } = req.params;
        const instrumentOption = await _instrument.selectInstrument(id);
        if (instrumentOption.isNone()) {
            throw new Error('Unable to select instrument');
        }
        return {
            meta: {
            },
            data: instrumentOption.unwrap()
        };
    });
    next();
};
exports.instrumentRoutes = instrumentRoutes;
