"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.exec = void 0;
var _childProcess = require("child_process");
var _util = require("util");
const exec = _util.promisify(_childProcess.exec);
exports.exec = exec;
