"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.replaceTildeWithHome = void 0;
const replaceTildeWithHome = (text)=>{
    return text.replace(/~/g, process.env.HOME);
};
exports.replaceTildeWithHome = replaceTildeWithHome;
