"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
var _replaceTildeWithHome = require("./replace-tilde-with-home");
Object.defineProperty(exports, "replaceTildeWithHome", {
    enumerable: true,
    get: function() {
        return _replaceTildeWithHome.replaceTildeWithHome;
    }
});
