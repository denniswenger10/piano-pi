"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.spawn = void 0;
var _childProcess = require("child_process");
var _makeSpawnCommand = require("./make-spawn-command");
const spawn = (text)=>{
    const [command, args] = _makeSpawnCommand.makeSpawnCommand(text);
    return _childProcess.spawn(command, args);
};
exports.spawn = spawn;
