"use strict";
var _makeSpawnCommand = require("./make-spawn-command");
describe('Make commands for spawn child process', ()=>{
    it('should give command and respect strings', ()=>{
        const command = 'fluidsynth -s -a alsa -g 1 -m alsa_seq "ladida dada da"';
        expect(_makeSpawnCommand.makeSpawnCommand(command)).toEqual([
            'fluidsynth',
            [
                '-s',
                '-a',
                'alsa',
                '-g',
                '1',
                '-m',
                'alsa_seq',
                'ladida dada da', 
            ], 
        ]);
    });
});
