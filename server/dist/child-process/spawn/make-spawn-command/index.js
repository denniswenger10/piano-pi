"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
var _makeSpawnCommand = require("./make-spawn-command");
Object.defineProperty(exports, "makeSpawnCommand", {
    enumerable: true,
    get: function() {
        return _makeSpawnCommand.makeSpawnCommand;
    }
});
