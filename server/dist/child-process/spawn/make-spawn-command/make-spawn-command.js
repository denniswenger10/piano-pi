"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.makeSpawnCommand = void 0;
var _replaceTildeWithHome = require("../replace-tilde-with-home");
const makeSpawnCommand = (text)=>{
    const stringSymbols = [
        '"',
        "'"
    ];
    let currentWord = '';
    let isInString = false;
    let command = '';
    let args = [];
    for (const letter of _replaceTildeWithHome.replaceTildeWithHome(text).split('')){
        if (stringSymbols.includes(letter)) {
            if (isInString) {
                if (command.length < 1) {
                    command = currentWord;
                } else {
                    args.push(currentWord);
                }
                currentWord = '';
            }
            isInString = !isInString;
            continue;
        }
        if (letter !== ' ' || isInString) {
            currentWord += letter;
            continue;
        }
        if (command.length < 1) {
            command = currentWord;
        } else {
            args.push(currentWord);
        }
        currentWord = '';
    }
    if (currentWord.length > 0) {
        if (command.length < 1) {
            command = currentWord;
        } else {
            args.push(currentWord);
        }
    }
    return [
        command,
        args
    ];
};
exports.makeSpawnCommand = makeSpawnCommand;
