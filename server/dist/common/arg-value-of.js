"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.argValueOf = void 0;
var _option = require("../option");
const argValueOf = (lookupArgs, args)=>{
    for (const arg of lookupArgs){
        const argIndex = args.indexOf(arg);
        if (argIndex > -1 && argIndex + 1 < args.length) {
            return _option.some(args[argIndex + 1]);
        }
    }
    return _option.none();
};
exports.argValueOf = argValueOf;
