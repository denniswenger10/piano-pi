"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.stringToRows = void 0;
const stringToRows = (text)=>{
    return text.split(/\r?\n/);
};
exports.stringToRows = stringToRows;
