"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.findWhere = void 0;
var _option = require("../option");
const findWhere = (predicate, list)=>{
    for (const item of list){
        if (predicate(item)) {
            return _option.some(item);
        }
    }
    return _option.none();
};
exports.findWhere = findWhere;
