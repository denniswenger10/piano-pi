"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.textIncludesAny = void 0;
const textIncludesAny = (searchList, text)=>{
    return searchList.some((s)=>text.includes(s)
    );
};
exports.textIncludesAny = textIncludesAny;
