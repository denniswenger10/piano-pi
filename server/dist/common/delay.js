"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.delay = void 0;
const delay = (ms = 1000)=>new Promise((resolve)=>setTimeout(resolve, ms)
    )
;
exports.delay = delay;
