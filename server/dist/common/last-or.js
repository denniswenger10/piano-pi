"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.lastOr = void 0;
var ref;
const lastOr = (or)=>(list)=>(ref = list[list.length - 1]) !== null && ref !== void 0 ? ref : or
;
exports.lastOr = lastOr;
