"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.uniqueId = void 0;
var _uuid = require("uuid");
const uniqueId = ()=>_uuid.v4()
;
exports.uniqueId = uniqueId;
