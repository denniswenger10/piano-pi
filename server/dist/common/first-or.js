"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.firstOr = void 0;
var ref;
const firstOr = (or)=>(list)=>(ref = list[0]) !== null && ref !== void 0 ? ref : or
;
exports.firstOr = firstOr;
