const replaceTildeWithHome = (text: string) => {
  return text.replace(/~/g, process.env.HOME)
}

export { replaceTildeWithHome }
