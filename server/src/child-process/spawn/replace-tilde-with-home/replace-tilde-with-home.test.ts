import { replaceTildeWithHome } from './replace-tilde-with-home'

describe('Replacing tilde with home path', () => {
  const path = '/downloads/fluid-soundfont-3.1/FluidR3_GM.sf2'

  it('should give home path instead of the tilde', () => {
    expect(replaceTildeWithHome(`~${path}`)).toBe(
      `${process.env.HOME}${path}`
    )
  })

  it('should do nothing', () => {
    expect(replaceTildeWithHome(path)).toBe(path)
  })
})
