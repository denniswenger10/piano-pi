import { spawn as _spawn } from 'child_process'
import { makeSpawnCommand } from './make-spawn-command'

const spawn = (text: string) => {
  const [command, args] = makeSpawnCommand(text)

  return _spawn(command, args)
}

export { spawn }
