const environment = () => {
  return {
    pianoPort: 9800,
    // soundfontPath: `~/downloads/fluid-soundfont-3.1/FluidR3_GM.sf2`,
    soundfontPaths: [`~/downloads/EssentialKeys-sforzando-v9.6.sf2`],
  }
}

export { environment }
