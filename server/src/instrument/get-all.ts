import { instrumentState } from './instrument-state'
import { fluidsynth } from '../piano'

const getAll = async () => {
  const cacheList = instrumentState.instrumentList.read()

  if (cacheList.length < 1) {
    const connection = await fluidsynth()
    const fonts = await connection.fonts()
    const instruments = await connection.instrumentsFromFont(
      fonts[0].id
    )
    instrumentState.instrumentList.update(instruments)
  }

  return instrumentState.instrumentList.read()
}

export { getAll }
