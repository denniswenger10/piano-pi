import { instrumentState } from './instrument-state'
import { findWhere } from '../common'
import { none, some } from '../option'
import { fluidsynth } from '../piano'
import { Instrument } from './create-instrument'

const selectInstrument = async (id: string) => {
  const instrumentOption = findWhere(
    ({ id: _id }) => _id === id,
    instrumentState.instrumentList.read()
  )

  if (instrumentOption.isNone()) {
    return none<Instrument>()
  }

  const instrument = instrumentOption.unwrap()
  console.time('Fluidsynth')
  const connection = await fluidsynth()
  console.timeEnd('Fluidsynth')

  console.time('Select instrument')
  await connection.selectInstrument(instrument)
  console.timeEnd('Select instrument')

  instrumentState.selectedInstrument.update(some(instrument))

  return some(instrument)
}

export { selectInstrument }
