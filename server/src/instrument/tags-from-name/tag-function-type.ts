import { Option } from '../../option'

type TagFunctionType = (name?: string) => Option<string>

export type { TagFunctionType }
