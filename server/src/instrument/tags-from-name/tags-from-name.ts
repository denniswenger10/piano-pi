import { filterMap } from '../../option'
import {
  fretsTag,
  keyboardTag,
  noiseTag,
  orchestraTag,
} from './tag-functions'
import { TagFunctionType } from './tag-function-type'

const tagsFromName = (name = ''): string[] => {
  const functions: TagFunctionType[] = [
    fretsTag,
    keyboardTag,
    noiseTag,
    orchestraTag,
  ]

  const lowerCaseName = name.toLowerCase()

  return filterMap((fn) => fn(lowerCaseName), functions)
}

export { tagsFromName }
