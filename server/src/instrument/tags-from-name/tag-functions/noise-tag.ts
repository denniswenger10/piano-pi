import { none, some } from '../../../option'
import { TagFunctionType } from '../tag-function-type'
import { textIncludesAny } from '../../../common'

const noiseTag: TagFunctionType = (name) => {
  return textIncludesAny(
    [
      'bird',
      'telephone',
      'gun',
      'applause',
      'noise',
      'helicopter',
      'sea shore',
    ],
    name
  ) && !textIncludesAny(['guitar', 'synth'], name)
    ? some('noise')
    : none()
}

export { noiseTag }
