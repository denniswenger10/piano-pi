import { none, some } from '../../../option'
import { TagFunctionType } from '../tag-function-type'
import { textIncludesAny } from '../../../common'

const fretsTag: TagFunctionType = (name) => {
  return textIncludesAny(
    [
      'guitar',
      'banjo',
      'ukulele',
      'mandol',
      'clavinet',
      'celesta',
      'harpsichord',
      'honky tonk',
      'honky-tonk',
      'bass',
    ],
    name
  ) &&
    !textIncludesAny(
      ['synth', 'bassoon', 'strings', 'sax', 'tuba', 'contra'],
      name
    )
    ? some('frets')
    : none()
}

export { fretsTag }
