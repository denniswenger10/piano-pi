import { none, some } from '../../../option'
import { TagFunctionType } from '../tag-function-type'
import { textIncludesAny } from '../../../common'

const keyboardTag: TagFunctionType = (name) => {
  return textIncludesAny(
    [
      'piano',
      'grand',
      'organ',
      ' ep',
      'clavinet',
      'celesta',
      'harpsichord',
      'honky tonk',
      'honky-tonk',
    ],
    name
  )
    ? some('keyboard')
    : none()
}

export { keyboardTag }
