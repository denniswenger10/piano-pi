import { none, some } from '../../../option'
import { TagFunctionType } from '../tag-function-type'
import { textIncludesAny } from '../../../common'

const orchestraTag: TagFunctionType = (name) => {
  return textIncludesAny(
    [
      'viol',
      'string',
      'orchest',
      'flute',
      'horn',
      'trombone',
      'tuba',
      'trumpet',
      'brass',
      'pizzicato',
      'cello',
      'contra',
      'oboe',
      'clarinet',
      'piccolo',
    ],
    name
  ) && !textIncludesAny(['guitar', 'synth'], name)
    ? some('orchestra')
    : none()
}

export { orchestraTag }
