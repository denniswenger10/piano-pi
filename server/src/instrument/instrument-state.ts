import { none } from '../option'
import { Instrument } from './create-instrument'
import { Option } from '../option'

interface State {
  instrumentList: Instrument[]
  selectedInstrument: Option<Instrument>
}

const state: State = {
  instrumentList: [],
  selectedInstrument: none(),
}

const instrumentState = {
  instrumentList: {
    read: () => state.instrumentList,
    update: (list: Instrument[]) => (state.instrumentList = list),
  },
  selectedInstrument: {
    read: () => state.selectedInstrument,
    update: (instrument: Option<Instrument>) =>
      (state.selectedInstrument = instrument),
  },
}

export { instrumentState }
