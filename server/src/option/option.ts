interface Option<DataType> {
  /** Returns value if `Option` is `some`. Will throw if `Option` is `none`. */
  unwrap(): DataType
  /** Returns value of `Option` if it is `some`. If `Option` is `none` it will return the value of the `or` parameter. */
  unwrapOr(or: DataType): DataType
  /** Returns value of `Option` if it is `some`. If `Option` is `none` it will run the `orElse` function. */
  unwrapOrElse(orElse: () => DataType): DataType
  match<MatchExpressionType>(matchers: {
    some(data: DataType): MatchExpressionType
    none(): MatchExpressionType
  }): MatchExpressionType
  /** Returns `true` if the `Option` is `some`. */
  isSome(): boolean
  /** Returns `true` if the `Option` is `none`. */
  isNone(): boolean
  /** Run a function that replaces the value of `some`. Will do nothing on `none` */
  map<FnReturnType>(
    fn: (data: DataType) => FnReturnType
  ): Option<FnReturnType>
  /** Run a function that replaces the value of `some`. Will replace `none` with `some` holding `or`. */
  mapOr<FnReturnType>(
    fn: (data: DataType) => FnReturnType,
    or: FnReturnType
  ): FnReturnType
  /** Run a function that replaces the value of `some`. Will replace `none` with `some` holding return value of `orElse` function. */
  mapOrElse<FnReturnType>(
    fn: (data: DataType) => FnReturnType,
    orElse: () => FnReturnType
  ): FnReturnType
  /** Returns `none` if `Option` is `none`. Will return `some` with value `predicate` is `true`, else it will return `none`.  */
  filter(predicate: (data: DataType) => boolean): Option<DataType>
  /** Returns a new `Option` with the original value and the value of provided option. If either option is `none` it will return `none`. */
  zip<SecondDataType>(
    option: Option<SecondDataType>
  ): Option<[DataType, SecondDataType]>
  // TODO: implement `andThen`
  // andThen<T>(fn: (data: DataType) => Option<T>): Option<T>
}

export type { Option }
