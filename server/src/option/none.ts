import { addBoolMethods } from './add-bool-methods'
import { Option } from './option'

const none = <DataType>(): Option<DataType> => {
  return {
    unwrap: () => {
      throw new Error(
        "No data here. Don't use unwrap unless you know it's completely safe."
      )
    },
    unwrapOr: (or) => or,
    unwrapOrElse: (orElse) => orElse(),
    match: ({ none: _none }) => _none(),
    map: <FunctionReturnType>(
      _: (d: DataType) => FunctionReturnType
    ) => none<FunctionReturnType>(),
    mapOr: <FunctionReturnType>(
      _: (d: DataType) => FunctionReturnType,
      or: FunctionReturnType
    ) => or,
    mapOrElse: <FunctionReturnType>(
      _: (d: DataType) => FunctionReturnType,
      orElse: () => FunctionReturnType
    ) => orElse(),
    filter: () => none<DataType>(),
    zip: <SecondDataType>(_: Option<SecondDataType>) =>
      none<[DataType, SecondDataType]>(),
    // andThen: <T>(_: (d: DataType) => Option<T>) => none<T>(),
    ...addBoolMethods(false),
  }
}

export { none }
