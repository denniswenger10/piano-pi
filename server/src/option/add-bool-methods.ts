const addBoolMethods = (isSome: boolean) => ({
  isSome: () => isSome,
  isNone: () => !isSome,
})

export { addBoolMethods }
