import { addBoolMethods } from './add-bool-methods'
import { Option } from './option'
import { none } from './none'

const some = <DataType>(data: DataType): Option<DataType> => {
  return {
    unwrap: () => data,
    unwrapOr: () => data,
    unwrapOrElse: () => data,
    match: ({ some: _some }) => _some(data),
    map: (fn) => some(fn(data)),
    mapOr: (fn) => fn(data),
    mapOrElse: (fn) => fn(data),
    filter: (fn) => (fn(data) ? some(data) : none<DataType>()),
    zip: (secondData) => secondData.map((x) => [data, x]),
    // andThen: (fn) => fn(data).map((_data) => _data),
    ...addBoolMethods(true),
  }
}

export { some }
