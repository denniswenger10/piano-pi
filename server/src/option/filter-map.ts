import { Option } from './option'

const filterMap = <DataType, ReturnType>(
  fn: (item: DataType) => Option<ReturnType>,
  list: DataType[]
): ReturnType[] => {
  const returnList: ReturnType[] = []

  for (const item of list) {
    fn(item).match({
      some(data) {
        returnList.push(data)
      },
      none() {},
    })
  }

  return returnList
}

export { filterMap }
