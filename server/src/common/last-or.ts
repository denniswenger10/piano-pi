const lastOr = <T>(or: T) => (list: T[]): T =>
  list[list.length - 1] ?? or

export { lastOr }
