import { Option, some, none } from '../option'

const findWhere = <T>(
  predicate: (item: T) => boolean,
  list: T[]
): Option<T> => {
  for (const item of list) {
    if (predicate(item)) {
      return some(item)
    }
  }

  return none()
}

export { findWhere }
