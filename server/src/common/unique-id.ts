import { v4 } from 'uuid'

const uniqueId = () => v4() as string

export { uniqueId }
