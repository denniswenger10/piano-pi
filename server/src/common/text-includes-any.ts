const textIncludesAny = (
  searchList: string[],
  text: string
): boolean => {
  return searchList.some((s) => text.includes(s))
}

export { textIncludesAny }
