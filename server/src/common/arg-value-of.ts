import { some, none, Option } from '../option'

const argValueOf = (
  lookupArgs: string[],
  args: string[]
): Option<string> => {
  for (const arg of lookupArgs) {
    const argIndex = args.indexOf(arg)

    if (argIndex > -1! && argIndex + 1 < args.length) {
      return some(args[argIndex + 1])
    }
  }

  return none()
}

export { argValueOf }
