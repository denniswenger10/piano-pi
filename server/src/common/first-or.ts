const firstOr = <T>(or: T) => (list: T[]): T => list[0] ?? or

export { firstOr }
