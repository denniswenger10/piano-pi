import { FastifyInstance } from 'fastify'
import {
  getAll,
  selectInstrument,
  instrumentState,
  createInstrument,
} from '../../instrument'

interface Params {
  id: string
}

const instrumentRoutes = (instance: FastifyInstance, _, next) => {
  instance.get('/', async () => {
    return { meta: {}, data: await getAll() }
  })

  instance.get('/select', async () => {
    return {
      meta: {},
      data: instrumentState.selectedInstrument
        .read()
        .unwrapOr(createInstrument()),
    }
  })

  instance.get<{ Params: Params }>('/select/:id', async (req) => {
    const { id } = req.params

    const instrumentOption = await selectInstrument(id)

    if (instrumentOption.isNone()) {
      throw new Error('Unable to select instrument')
    }

    return { meta: {}, data: instrumentOption.unwrap() }
  })

  next()
}

export { instrumentRoutes }
