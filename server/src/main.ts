import fastify from 'fastify'
import fastifyStatic from 'fastify-static'
import fastifyCors from 'fastify-cors'
import path from 'path'
import { instrumentRoutes } from './api/v1'
import { startPiano } from './piano'
import { environment } from './environment'

const main = async () => {
  const pianoPort = environment().pianoPort
  const soundfonts = environment().soundfontPaths

  await startPiano(soundfonts, pianoPort)

  const app = fastify()

  app.register(fastifyCors)

  app.register(fastifyStatic, {
    root: path.join(__dirname, '../../client', 'build'),
    prefix: '/',
  })

  app.register(instrumentRoutes, { prefix: '/api/v1/instruments' })

  const port = 3000

  const start = async () => {
    try {
      await app.listen(port)
      console.log(`Server started at ${port}`)
    } catch (err) {
      app.log.error(err)
      // process.exit(1)
    }
  }
  start()
}

main()
