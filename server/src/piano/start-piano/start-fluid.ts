import { ChildProcessWithoutNullStreams } from 'child_process'
import { spawn } from '../../child-process'

const startFluid = (
  soundfontPaths: string[],
  port: number
): Promise<ChildProcessWithoutNullStreams> => {
  return new Promise((resolve, reject) => {
    const fluidsynthProcess = spawn(
      `fluidsynth -s -a alsa -g 1 -c 2 -z 128 -r 48000 -o shell.port=${port} -m alsa_seq ${soundfontPaths.join(
        ' '
      )}`
    )

    fluidsynthProcess.stderr.on('data', (data) => {
      console.error(`fluidsynth: ${data}`)
      reject(`fluidsynth error: ${data}`)
    })

    fluidsynthProcess.on('close', () => {
      console.log('Fluidsynth closed')
    })

    fluidsynthProcess.on('exit', () => {
      console.log('Fluidsynth closed')
    })

    fluidsynthProcess.stdout.on('data', (data: string) => {
      if (data.includes('>')) {
        resolve(fluidsynthProcess)
      }
    })
  })
}

export { startFluid }
