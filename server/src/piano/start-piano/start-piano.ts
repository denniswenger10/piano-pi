import { exec } from '../../child-process'
import {
  getAll as getAllInstruments,
  selectInstrument,
} from '../../instrument'
import { startFluid } from './start-fluid'
import md5 from 'md5-file'

const startPiano = async (
  soundfontPaths: string[] = [
    `~/downloads/fluid-soundfont-3.1/FluidR3_GM.sf2`,
  ],
  port: number = 9800
) => {
  const fluidsynthProcess = await startFluid(soundfontPaths, port)
  await md5(soundfontPaths[0])

  console.log('Fluidsynth started')

  const { stderr } = await exec('aconnect 20:0 128:0')

  if (stderr.length > 0) {
    console.log('This should be a problem')
  }

  console.log('Piano and fluidsynth connected')

  const instruments = await getAllInstruments()

  if (instruments.length > 0) {
    await selectInstrument(instruments[0].id)
  }

  return fluidsynthProcess
}

export { startPiano }
