import { createSoundfont, Soundfont } from './create-soundfont'
import { stringToRows } from '../../../common'
import { Option, filterMap, none, some } from '../../../option'

const parseRow = (row: string): Option<Soundfont> => {
  const [id, ...name] = row.trim().split(' ')

  if (isNaN(parseInt(id))) {
    return none()
  }

  const soundfont = createSoundfont({
    id,
    name: name.join(''),
  })

  return [soundfont.id.length > 0, soundfont.name.length > 0].every(
    (x) => x
  )
    ? some(soundfont)
    : none()
}

const parseFonts = (fontText: string): Soundfont[] => {
  const [, ...rows] = stringToRows(fontText)

  return filterMap(parseRow, rows)
}

export { parseFonts }
