import { parseFonts } from './parse-fonts'

describe('Parse fonts from fluidsynth', () => {
  it('should parse into object', () => {
    const data = `
ID  Name
 1  /home/dennis/downloads/fluid-soundfont-3.1/FluidR3_GM.sf2
`
    expect(parseFonts(data)).toEqual([
      {
        id: '1',
        name:
          '/home/dennis/downloads/fluid-soundfont-3.1/FluidR3_GM.sf2',
      },
    ])
  })
})

/*

ID  Name
 1  /home/dennis/downloads/fluid-soundfont-3.1/FluidR3_GM.sf2

*/
