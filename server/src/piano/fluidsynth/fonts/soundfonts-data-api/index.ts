import { readJson, pathExists } from 'fs-extra'
import path from 'path'
import { none, Option, some } from '../../../../option'
import {
  createSoundfontData,
  SoundfontData,
} from './create-soundfont-data'

const fontDataDirPath = path.join(__dirname, '../data')

const createPathToFile = (id: string) =>
  path.join(fontDataDirPath, `${id}.json`)

const read = async (id: string): Promise<Option<SoundfontData>> => {
  const filePath = createPathToFile(id)

  return (await pathExists(filePath))
    ? some(createSoundfontData(await readJson(filePath)))
    : none()
}

const update = async () => {}

const create = async (soundfontData: SoundfontData) => {}
