import {
  Instrument,
  MaybeInstrument,
  createInstrument,
} from '../../../../instrument'

interface SoundfontData {
  name: string
  md5: string
  instruments: Instrument[]
}

interface MaybeSoundfontData {
  name?: string
  md5?: string
  instruments?: MaybeInstrument[]
}

const createSoundfontData = (
  data: MaybeSoundfontData
): SoundfontData => {
  const { name = '', md5 = '', instruments = [] } = data

  return {
    name,
    md5,
    instruments: instruments.map((x = {}) => createInstrument(x)),
  }
}

export { createSoundfontData }
export type { SoundfontData, MaybeSoundfontData }
