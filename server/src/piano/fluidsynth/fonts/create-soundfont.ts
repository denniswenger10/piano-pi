interface MaybeSoundfont {
  name?: string
  id?: string
}

interface Soundfont {
  name: string
  id: string
}

const createSoundfont = (
  soundfont: MaybeSoundfont = {}
): Soundfont => {
  const { name = '', id = '' } = soundfont

  return {
    name,
    id,
  }
}

export { createSoundfont }
export type { Soundfont }
