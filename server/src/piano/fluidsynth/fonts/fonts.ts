import { FluidsynthConnection } from '../fluidsynth-connection'
import { parseFonts } from './parse-fonts'

const fonts = (connection: FluidsynthConnection) => async () => {
  const resp = await connection.send('fonts')

  return parseFonts(resp)
}

export { fonts }
