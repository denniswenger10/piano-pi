import { fluidsynthConnection } from './fluidsynth-connection'
import { fonts } from './fonts'
import { instrumentsFromFont } from './instruments'
import { selectInstrument } from './select-instrument'

const fluidsynth = async (port: number = 9800) => {
  const connection = await fluidsynthConnection(port)

  return {
    fonts: fonts(connection),
    instrumentsFromFont: instrumentsFromFont(connection),
    selectInstrument: selectInstrument(connection),
  }
}

export { fluidsynth }
