import { FluidsynthConnection } from '../fluidsynth-connection'
import { parseInstruments } from './parse-instruments'

const instrumentsFromFont = (
  connection: FluidsynthConnection
) => async (fontId: string) => {
  const resp = await connection.send(`inst ${fontId}`)

  return parseInstruments(resp)
}

export { instrumentsFromFont }
