import { tagsFromName } from '../../../instrument'

const getTagsFromRow = (name: string): string[] => tagsFromName(name)

export { getTagsFromRow }
