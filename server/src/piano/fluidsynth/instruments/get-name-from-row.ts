const getNameFromRow = (row: string): string => {
  const [, ...tail] = row.split(' ')
  return tail.join(' ')
}

export { getNameFromRow }
