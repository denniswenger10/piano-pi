import { firstOr } from '../../../common'

const getBankFromRow = (row: string): number =>
  +firstOr('0')(row.split('-'))

export { getBankFromRow }
