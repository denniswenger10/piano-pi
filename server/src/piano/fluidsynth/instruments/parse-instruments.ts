import { stringToRows } from '../../../common'
import { createInstrument, Instrument } from '../../../instrument'
import { Option, some, none, filterMap } from '../../../option'
import { getBankFromRow } from './get-bank-from-row'
import { getNameFromRow } from './get-name-from-row'
import { getNumberFromRow } from './get-number-from-row'
import { getTagsFromRow } from './get-tags-from-row'

const parseRow = (text: string): Option<Instrument> => {
  const trimmed = text.trim()

  const bank = getBankFromRow(trimmed)
  const number = getNumberFromRow(trimmed)
  const name = getNameFromRow(trimmed)
  const tags = getTagsFromRow(name)

  const instrument = createInstrument({
    bank,
    number,
    name,
    tags,
  })

  return [instrument.name.length > 0].every((x) => x)
    ? some(instrument)
    : none()
}

const parseInstruments = (text: string) => {
  return filterMap(parseRow, stringToRows(text))
}

export { parseInstruments }
