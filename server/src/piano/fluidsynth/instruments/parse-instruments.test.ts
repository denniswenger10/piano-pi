import { parseInstruments } from './parse-instruments'

describe('Parse fonts from fluidsynth', () => {
  it('should parse into object', () => {
    const data = `
000-000 Yamaha Grand Piano
000-001 Bright Yamaha Grand
`

    const result = parseInstruments(data).map(
      ({ id, ...rest }) => rest
    )

    expect(result).toEqual([
      {
        name: 'Yamaha Grand Piano',
        number: 0,
        bank: 0,
        tags: [],
      },
      {
        name: 'Bright Yamaha Grand',
        number: 1,
        bank: 0,
        tags: [],
      },
    ])
  })
})
