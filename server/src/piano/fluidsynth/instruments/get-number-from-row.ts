import { firstOr } from '../../../common'

const getNumberFromRow = (row: string): number =>
  +(firstOr('0-0')(row.split(' ')).split('-')[1] ?? '0')

export { getNumberFromRow }
