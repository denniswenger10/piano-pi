import { Instrument } from '../../../instrument'
import { FluidsynthConnection } from '../fluidsynth-connection'
import { delay } from '../../../common'

const selectInstrument = (connection: FluidsynthConnection) => async (
  instrument: Instrument
) => {
  connection.send(
    `select 0 0 ${instrument.bank} ${instrument.number}`
  )

  await delay(5)

  return ''
}

export { selectInstrument }
