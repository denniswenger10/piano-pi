import Telnet from 'telnet-client'

let connection: Telnet = null

interface FluidsynthConnection {
  send: (text: string) => Promise<string>
  close: () => Promise<void>
}

const createInterface = (
  connection: Telnet
): FluidsynthConnection => {
  return {
    send: async (text: string) => {
      const resp = await connection.send(text)
      return resp.replace(/(?:\r\n> |\r> |\n> |^(> ))/g, '')
    },
    close: () => connection.end(),
  }
}

const fluidsynthConnection = async (port: number = 9800) => {
  if (connection !== null) {
    return createInterface(connection)
  }

  connection = new Telnet()

  const params = {
    host: 'localhost',
    port,
    timeout: 1500,
    negotiationMandatory: false,
  }

  await connection.connect(params)

  return createInterface(connection)
}

export { fluidsynthConnection }
export type { FluidsynthConnection }
