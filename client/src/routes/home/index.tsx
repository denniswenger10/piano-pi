import { FunctionalComponent, h } from 'preact'
import { SimpleGrid } from '@chakra-ui/react'
import { useQuery, useQueryClient } from 'react-query'
import { apiRequest } from '../../common'
import { Instrument } from '../../model'
import { InstrumentItem } from '../../components/instrument-item'
import { ApiResponse } from '../../types'

const Home: FunctionalComponent = () => {
  const queryClient = useQueryClient()

  const {
    isLoading: isInstrumentListLoading,
    error: instrumentListError,
    data: instrumentListData,
  } = useQuery<ApiResponse<Instrument[]>, Error>(
    'instrumentList',
    () => apiRequest.get<ApiResponse<Instrument[]>>('/instruments')
  )
  const {
    isLoading: isSelectedLoading,
    error: selectedError,
    data: selectedData,
  } = useQuery<ApiResponse<Instrument>, Error>('select', () =>
    apiRequest.get<ApiResponse<Instrument>>('/instruments/select')
  )

  if (isInstrumentListLoading || isSelectedLoading)
    return <div>'Loading...'</div>

  if (instrumentListError)
    return (
      <div>An error has occurred: ${instrumentListError.message}</div>
    )
  if (selectedError)
    return <div>An error has occurred: ${selectedError.message}</div>

  if (instrumentListData === undefined || selectedData === undefined)
    return <div>Data was undefined</div>

  const { data: instrumentList } = instrumentListData
  const { data: selected } = selectedData

  const handleOnClick = async (id: string) => {
    console.log('Request sent')
    await apiRequest.get<ApiResponse<Instrument>>(
      `/instruments/select/${id}`
    )
    console.log('Request resolved')

    queryClient.invalidateQueries('select')
  }

  return (
    <SimpleGrid columns={3} gap="4" p="4">
      {instrumentList.map((instrument) => (
        <InstrumentItem
          instrument={instrument}
          isSelected={instrument.id === selected.id}
          onClick={() => handleOnClick(instrument.id)}
        />
      ))}
    </SimpleGrid>
  )
}

export default Home
