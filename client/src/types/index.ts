interface ApiResponse<DataType> {
  meta: string
  data: DataType
}

export type { ApiResponse }
