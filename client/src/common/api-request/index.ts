import { createRequest } from '../http'
import { environment } from '../../environment'

const createApiRequest = (method: string) => async <
  DataReturnType,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  DataBodyType extends Record<string, unknown> = any
>(
  endpoint: string,
  body?: DataBodyType
): Promise<DataReturnType> =>
  createRequest(method)<DataReturnType, DataBodyType>(
    `${environment().apiUrl}${endpoint}`,
    body
  )

const apiRequest = {
  get: createApiRequest('GET'),
  put: createApiRequest('PUT'),
  post: createApiRequest('POST'),
  patch: createApiRequest('PATCH'),
  delete: createApiRequest('DELETE'),
}

export { apiRequest }
