const createRequest = (method: string) => async <
  DataReturnType,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  DataBodyType extends Record<string, unknown> = any
>(
  url: string,
  body?: DataBodyType
): Promise<DataReturnType> => {
  const _body = body ? JSON.stringify(body) : undefined

  const res = await fetch(url, { method, body: _body })
  const data = await res.json()

  return data as DataReturnType
}

const http = {
  get: createRequest('GET'),
  put: createRequest('PUT'),
  post: createRequest('POST'),
  patch: createRequest('PATCH'),
  delete: createRequest('DELETE'),
}

export { http, createRequest }
