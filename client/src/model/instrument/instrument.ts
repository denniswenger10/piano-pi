interface Instrument {
  name: string
  tags: string[]
  bank: number
  number: number
  id: string
}

interface MaybeInstrument {
  name?: string
  tags?: string[]
  bank?: number
  number?: number
  id?: string
}

const createInstrument = (
  instrument: MaybeInstrument = {}
): Instrument => {
  const {
    name = '',
    bank = 0,
    tags = [],
    number = 0,
    id = '',
  } = instrument

  return {
    name,
    bank,
    tags,
    number,
    id,
  }
}

export { createInstrument }
export type { Instrument }
