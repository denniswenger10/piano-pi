import { FunctionalComponent, h } from 'preact'
import { Route, Router } from 'preact-router'
import { ChakraProvider } from '@chakra-ui/react'
import { QueryClient, QueryClientProvider } from 'react-query'

import Home from '../routes/home'
import NotFoundPage from '../routes/not-found-page'

const queryClient = new QueryClient()

const App: FunctionalComponent = () => {
  return (
    <div id="preact_root">
      <ChakraProvider>
        <QueryClientProvider client={queryClient}>
          <Router>
            <Route path="/" component={Home} />
            <NotFoundPage default />
          </Router>
        </QueryClientProvider>
      </ChakraProvider>
    </div>
  )
}

export { App }
