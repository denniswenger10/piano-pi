import { FunctionalComponent, h } from 'preact'
import { Button } from '@chakra-ui/react'
import { Instrument } from '../../model'

interface Props {
  instrument: Instrument
  isSelected?: boolean
  onClick?: (instrument: Instrument) => void
}

const InstrumentItem: FunctionalComponent<Props> = ({
  instrument,
  isSelected = false,
  onClick = () => {
    return
  },
}) => {
  const { name } = instrument

  return (
    <Button
      background={isSelected ? 'blue.500' : 'gray.200'}
      color={isSelected ? 'gray.50' : 'gray.900'}
      _hover={{
        background: isSelected ? 'blue.600' : 'gray.300',
      }}
      onClick={() => onClick(instrument)}
    >
      {name}
    </Button>
  )
}

export { InstrumentItem }
